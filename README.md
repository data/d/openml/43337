# OpenML dataset: HMEQ_Data

https://www.openml.org/d/43337

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The consumer credit department of a bank wants to automate the decisionmaking process for approval of home equity lines of credit. To do this, they will follow the recommendations of the Equal Credit Opportunity Act to create an empirically derived and statistically sound credit scoring model. The model will be based on data collected from recent applicants granted credit through the current process of loan underwriting. The model will be built from predictive modeling tools, but the created model must be sufficiently interpretable to provide a reason for any adverse actions (rejections). 
Content
The Home Equity dataset (HMEQ) contains baseline and loan performance information for 5,960 recent home equity loans. The target (BAD) is a binary variable indicating whether an applicant eventually defaulted or was seriously delinquent. This adverse outcome occurred in 1,189 cases (20). For each applicant, 12 input variables were recorded.
Acknowledgements
Inspiration
What if you can predict clients who default on their loans.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43337) of an [OpenML dataset](https://www.openml.org/d/43337). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43337/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43337/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43337/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

